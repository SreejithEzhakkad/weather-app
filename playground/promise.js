let addValues = (a ,b) => {
  return new Promise((resolve,reject) => {
    if(typeof a === "number" && typeof b === "number"){
      resolve(a+b);
    }else{
      reject("Paramete is not number");
    }
  });
};

addValues("20",20).then((response)=>{
  console.log(response);
  return addValues(response,50);
}).then((response) => {
  console.log(response);
}).catch((errorMessage)=>{
  console.log(errorMessage);
});
// let somePromise = new Promise((resolve,reject) => {
//   setTimeout(()=> {
//     //resolve('Its worked');
//     reject('not worked');
//   },2500)
// });
//
// somePromise.then((messsage) => {
//   console.log(messsage);
// },(errorMessage)=>{
//   console.error(errorMessage);
// });


// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     //resolve('Hey. It worked!');
//     reject('Unable to fulfill promise');
//   }, 2500);
// });
//
// somePromise.then((message) => {
//   console.log('Success: ', message);
// }, (errorMessage) => {
//   console.log('Error: ', errorMessage);
// });
