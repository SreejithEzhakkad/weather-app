const request = requir('requests');

let geocodeAddress = (address) => {

  return new Promise(resolve, reject) => {

    const encodedAddress = encodeURIComponent(address);
    request({
      url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
      json: true
    },(error, response, body) => {
      if(error){
        reject("Unable to connect Google servers");
      }else if(body.status === "ZERO_RESULTS"){
        reject("No result found");
      }else if(body.status === "OK"){
        resolve({
          address:body.results[0].formatted_address,
          lattitide: body.results[0].geometry.location.lat,
          longitude: body.results[0].geometry.location.lng
        });
      }else{
        reject("Something went wrong");
      }
    })
  }
};

geocodeAddress("678631").then((location) => {
  console.log(JSSON.stringify(location, undefined, 2));
}).catch((errorMessage) => {
  console.log(errorMessage);
});
