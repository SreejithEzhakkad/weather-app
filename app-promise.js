const request = require('request');
const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
  .options({
    a:{
      demand: true,
      alias: 'address',
      describe: 'Address to fetch weather',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv;

  const encodedAddress = encodeURIComponent(argv.address);
  const GoogleAPIKey = 'AIzaSyDVS-pSl9lvk9eZdWvfUXkmfDA3TAI-x38';
  const WeatherAPIKey = "b3b2900cc9d0262b90448a486ad2eca6";
  const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

  axios.get(url).then((response) => {

    if(response.data.status === "ZERO_RESULTS"){
      throw new Error("No result found");
    }
    if(response.data.status !== "OK"){
      throw new Error("Something went wrong");
    }
      console.log(response.data);
      //address:body.results[0].formatted_address,
      let lattitide = body.results[0].geometry.location.lat;
      let longitude = body.results[0].geometry.location.lng;
      return axios.get(`https://api.darksky.net/forecast/${WeatherAPIKey}/${lattitide},${longitude}`);


  }).then((response) => {
    let temperature = response.data.currently.temperature;
    console.log(`Current temp:${temperature}`);
  }).catch((e) => {
    if(e.code === 'ENOTFOUND'){
      console.log("The API server not available");
    }else{
      console.log(e.message);
    }
    //console.log(errorMessage);
  })
// geocode.geocodeAddress(argv.a, (errorMessage, results) => {
//   if(errorMessage){
//     console.log(errorMessage);
//   }else{
//     weather.getWeather(results.lattitide,results.longitude, (error, result) => {
//       if(error){
//         console.log(error);
//       }else{
//         console.log(result.temperature);
//       }
//     });
//     console.log(JSON.stringify(results,undefined,2));
//   }
// });
