const request = require('request');
const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
  .options({
    a:{
      demand: true,
      alias: 'address',
      describe: 'Address to fetch weather',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv;


geocode.geocodeAddress(argv.a, (errorMessage, results) => {
  if(errorMessage){
    console.log(errorMessage);
  }else{
    weather.getWeather(results.lattitide,results.longitude, (error, result) => {
      if(error){
        console.log(error);
      }else{
        console.log(result.temperature);
      }
    });
    console.log(JSON.stringify(results,undefined,2));
  }
});
