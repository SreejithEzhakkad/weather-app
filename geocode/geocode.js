const request = require('request');

geocodeAddress = (address,callback) => {
  const encodedAddress = encodeURIComponent(address);

  request({
  url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
  json: true
  },(error, response, body) => {
  if(error){
    callback("Unable to connect Google servers");
  }else if(body.status === "ZERO_RESULTS"){
    callback("No result found");
  }else if(body.status === "OK"){
    callback(undefined,{
      address:body.results[0].formatted_address,
      lattitide: body.results[0].geometry.location.lat,
      longitude: body.results[0].geometry.location.lng
    })
  }else{
    callback("Something went wrong");
  }

  })
};


module.exports.geocodeAddress = geocodeAddress;
